import HomeNewsBanner from "components/home/HomeNewsBanner";
import ImageFixed from "components/ui/ImageFixed";
import Link from "next/link";
import useHomeInit from "hooks/useHomeInit";
import type { NextPage } from "next";
import Head from "next/head";
import styled from "styled-components";

const MainBottom = styled.div`
    background-color: var(--green-emeraude-975);
    margin-bottom: 0.2rem;
    width: 100%;
    padding: 1.25rem;
    display: flex;
    justify-content: center;
    align-items: center;

    @media (min-width: 768px) {
        padding: 5rem 10rem;
    }
    @media (min-width: 1024px) {
        padding: 5rem 15rem;
    }
`;
const MainBottomBody = styled.div`
    background-color: var(--grey-1000);
    padding: 32px;
    text-align: center;
    width: 100vw;
    max-width: 56rem;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    @media (min-width: 768px) {
        flex-direction: row;
    }

    @media (min-width: 940px) {
        padding: 50px 40px;
        text-align: left;
    }
`;

const StyledBottomLogoContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-bottom: 3.5rem;

    @media (min-width: 768px) {
        margin-right: 3.5rem;
        margin-bottom: 0;
    }
`;

const StyledLandingIntroductionContainer = styled.div`
    border: 2px solid var(--yellow-tournesol-950-100);
`;

const Home: NextPage = () => {
    useHomeInit();

    return (
        <>
            <Head>
                <title>Dotations Locales</title>
            </Head>
            <HomeNewsBanner />
            <div className="flex flex-col items-center px-2 md:px-0">
                <div className="pt-5 md:pt-20 text-center">
                    <h1 className="p-0 m-0">
                        Consultez et analysez les dotations locales de <br />
                        votre collectivité territoriale
                    </h1>
                </div>
                
                <StyledLandingIntroductionContainer className="flex py-6 px-6 sm:px-8 items-center mx-auto mt-10 mb-20 max-w-4xl">
                    <div>
                        Chers usagers,
                        <br />
                        <br />
                        Nous vous remercions pour votre confiance et votre fidélité au service Dotations Locales.
                        <br />
                        Nous vous informons que le site fermera ses portes.
                        <br />
                        À partir de janvier 2024, vous pourrez retrouver les dotations d&apos;État aux territoires 
                        sur  <Link
                            href="http://www.dotations-dgcl.interieur.gouv.fr/consultation/accueil.php"
                            target="_"
                        >le site officiel de la Direction générale des collectivités locales (DGCL)</Link>
                        .
                        <br />
                        <br />
                        Nous vous souhaitons une bonne continuation.
                        <br />
                        L&apos;équipe Dotations Locales de l&apos;Incubateur des Territoires de l&apos;Agence nationale de la cohésion des territoires (ANCT).
                    </div>
                </StyledLandingIntroductionContainer>

                <MainBottom>
                    <MainBottomBody>
                        <StyledBottomLogoContainer>
                            <ImageFixed
                                className="mb-4"
                                src="/images/france-relance.png"
                                height={116}
                                width={116}
                                alt="icone france relance"
                            />
                            <ImageFixed
                                src="/images/financé-ue.png"
                                height={41.5}
                                width={179}
                                alt="icone union européenne"
                            />
                        </StyledBottomLogoContainer>

                        <div>
                            <div className="text-2xl font-bold mb-4">
                                Service soutenu par le plan France Relance
                            </div>
                            <span>
                                Le service Dotations locales est né du constat d&apos;une
                                évolution des concours financiers aux budgets
                                des territoires et de l&apos;absence
                                d&apos;outils partagés permettant d&apos;en
                                anticiper les effets. Or, les données ouvertes
                                sur les territoires ainsi que la naissance
                                d&apos;un modèle open source des règles de
                                calcul des dotations rendent cela possible.
                            </span>
                        </div>
                    </MainBottomBody>
                </MainBottom>
            </div>
        </>
    );
};

export default Home;
