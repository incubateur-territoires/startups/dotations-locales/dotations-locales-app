import { Badge } from "@dataesr/react-dsfr";
import useResize from "hooks/useResize";
import Link from "next/link";
import styled from "styled-components";

const StyledContainer = styled.div`
    background: var(--yellow-tournesol-950-100);
    width: 100%;
    display: flex;
    justify-content: center;
    padding: 20px 8px;

    @media (min-width: 900px) {
        padding: 20px 0;
    }
`;

export default function HomeNewsBanner() {
    const { windowWidth } = useResize();
    return (
        <StyledContainer>
            <Badge text="FIN DE SERVICE" type="warning" isSmall={windowWidth < 768} hasIcon />
            <div className="ml-2 text-color-primary font-bold md:text-base text-sm">
                <span>
                    En janvier 2024, le service Dotations Locales de l&apos;Agence nationale de la cohésion des territoires prend fin.{" "}
                </span>
                <br />
                <span>
                    <Link
                        href="http://www.dotations-dgcl.interieur.gouv.fr/consultation/dotations_en_ligne.php"
                        target="_"
                    >
                        Cliquer ici
                    </Link>
                    {" "}pour retrouver les dotations d&apos;État sur le site officiel de la Direction générale des collectivités locales.
                </span>
            </div>
        </StyledContainer>
    );
}
