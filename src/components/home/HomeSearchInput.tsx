import { SearchInput } from "components/ui";
import useResize from "hooks/useResize";

const HomeSearchInput = () => {
    const { windowWidth } = useResize();
    // TODO passer windowWidth en property

    return (
        <div>
            <div className="my-6 text-center px-10 md:px-0">
                <span className="text-lg md:font-bold">
                    Recherchez votre commune, intercommunalité ou
                    département
                </span>
            </div>
            <SearchInput
                placeholder={`${windowWidth > 480 ? "Recherche par n" : "N"
                    }om, code insee ou code postal`}
                textIcon="Rechercher"
            />
        </div>
    );
};

export default HomeSearchInput;
