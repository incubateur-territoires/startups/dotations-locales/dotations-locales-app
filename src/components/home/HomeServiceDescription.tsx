import HomeRowGuideDownload from "components/home/HomeRowGuideDownload";
import HomeRowImageText from "components/home/HomeRowImageText";
import LandingIntroduction from "components/home/LandingIntroduction";

const HomeServiceDescription = () => {
    return (
        <div className="md:mx-60 text-center md:text-left">
            <LandingIntroduction />
            <HomeRowGuideDownload />

            <HomeRowImageText
                priority
                src="/images/landing-euro.png"
                imageHeight={444}
                imageWidth={444}
                imageAlt="first row image"
                titleContent="Améliorons la connaissance sur les dotations d'État"
            >
                Le Service
                <strong> Dotations Locales</strong> a pour objectif de
                rendre accessible et de faciliter l&apos;exploitation
                des montants attribués au titre de la dotation globale
                de fonctionnement de votre collectivité.
            </HomeRowImageText>

            <HomeRowImageText
                src="/images/landing-dotations.png"
                reverse
                imageHeight={444}
                imageWidth={444}
                imageAlt="second row image"
                titleContent="Les données utiles pour comprendre votre budget"
            >
                Accédez rapidement aux informations et aux montants dont
                vous avez besoin pour établir votre budget. <br />{" "}
                <br />
                <strong>
                    Exportez et utilisez les données comme vous le
                    souhaitez !
                </strong>
            </HomeRowImageText>

            <HomeRowImageText
                src="/images/landing-chart.png"
                imageHeight={320}
                imageWidth={320}
                imageAlt="third row image"
                titleContent="Comparez et analysez l'évolution de vos dotations"
            >
                Un outil pratique pour suivre l&apos;évolution des
                montants, comparer et analyser les critères et les
                données qui ont un impact fort sur les fonds qui vous
                sont attribués.
            </HomeRowImageText>

            <HomeRowImageText
                reverse
                src="/images/landing-open-fisca.png"
                imageHeight={256}
                imageWidth={256}
                imageAlt="fourth row image"
                titleContent="Sur la base du moteur de calcul OpenFisca"
                badgeText="en beta"
                badgeType="new"
                badgeHasIcon
            >
                <strong>Dotations Locales</strong> s&apos;appuie sur{" "}
                <strong>OpenFisca</strong>, un moteur de calcul libre,
                collaboratif et transparent qui permet de simuler
                l&lsquo;impact de réformes sur les dotations des
                collectivités.
            </HomeRowImageText>

            <HomeRowImageText
                src="/images/landing-data-gouv.png"
                imageHeight={256}
                imageWidth={256}
                imageAlt="fifth row image"
                titleContent="Des données libres et partagées en Open Data"
            >
                L&apos;ensemble des données de votre collectivité sont
                collectées automatiquement à partir des plateformes
                publiques
                <strong> Data.gouv.fr.</strong>
            </HomeRowImageText>
        </div>
    );
};

export default HomeServiceDescription;
